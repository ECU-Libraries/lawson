---
layout : ecu
title : How Plants Are Named - John Lawson Exhibit
---

{% include breadcrumbs.html active="" %}

<h2>How Plants Are Named with Some Examples Selected from the John Lawson Plant Collection</h2>

<p>
Planet Earth supports thousands of different plant species. The same species may have different names in different languages. The name for our common Dandelion is an anglicized corruption of the French "dent-de-leon," or lion’s tooth. The German "Lindenbaum" is called "Basswood" in English. The same plant may be given different common names even within one country. A common American shrub, covered in Autumn with red-seeded orange pods, is known to most Americans as Strawberry Bush. Folks in the Appalachian Mountains call this same shrub "Hearts-a-bustin." Finding the correct name for a plant during the colonial period in America was further complicated by the fact that immigrants often applied European names to American plants that resembled familiar plants from home, but which were really different species.<sup>1</sup> A common English tree is called a "Sycamore." It belongs to the maple family. When English colonists arrived in America they began calling a tree with similar leaves, "Sycamore." It turns out that the American sycamore belongs to an entirely different family. Many colonists simply used the local Native-American names. Examples of Native-American names include Yaupon, Chinquapin, Pawpaw, Ipomoea, Titi, Puccoon, and Sassafras. Still other plants indicate Native-American associations. These names include Indian Cucumber Root and Squaw Huckleberry. All of the above-named plants are represented among the Lawson Plants.
</p>
<p>
Plants have always been of economic importance to humans. Plants provide food and medicine. We make our homes and clothing from them while poets, artists, and gardeners receive inspiration from their beauty. Even in prehistory it was necessary to name plants correctly. Mistaking a poisonous plant for an edible one could have fatal results. Over two thousand years ago, Greek physicians began to produce plant identification books, called herbals. The books contained illustrations and brief descriptions of useful plants. I have found modern equivalents of these ancient books for sale at herb markets in Costa Rica. The problem of differing common names among the several languages was addressed during medieval times by writing plant herbals in the scholarly language of Latin.<sup>2</sup> While it may be reasonable to expect a physician or naturalist to memorize a few hundred descriptions of useful plants, the task became unmanageable during the Age of Exploration when collectors, like John Lawson, began sending thousands of specimens back to Europe from all parts of the World.
</p>
<p>
Nearly half a century after John Lawson’s death, Carl Linnaeus developed a
system that greatly facilitated the process for classifying plants. Consider
the information given in the footnote below.<sup>3</sup> The Latin words and
numbers refer to published descriptions of plants that resemble the mounted
specimen. The first entry compares this plant to Candy Cassidony as appears in
a book by John Ray. John Ray made the first attempt to describe all known
plants. He published <em>Historia Plantarum</em> in three volumes between 1686 and 1704.
There is evidence that James Petiver gave John Lawson a copy of Ray’s book when
Lawson was in England in 1708. The last entry in this footnote is in a
classical pre-Linnaean format. It reads Staechas citrina globosa et amplo flore
Cretica. A loose translation might read: "This plant is illustrated in plate 987
and is described on page 814 of a book by Burrelier that describes the flora of
Crete. The plant bears abundant rounded heads of pale yellow flowers." This
string of Latin is known as a Latin polynomial. Linnaeus’ innovation was to
reduce all of this information to an abbreviated form. Linnaeus created a
"scientific name" consisting of the first two words in the Latin description,
<em>Staechas citrina</em>, together with the author of the description of that plant.
This combination is called a Latin binomial. Linnaeus assumed that all serious
naturalists would have access to the existing botanical literature and that
they could use the "short description" to look up the detailed original. Thus,
if two authors gave different names to the same plant the interested reader
could compare <em>Candy cassidony</em> Ray with <em>Staechas citrina</em> Bourrelier
or <em>Elychrysum criticum</em> CB. All three of these scientific names are correct
Linnaean binomials.
The book's authors are given as a helpful reference. Author names are referred to
as the authority. An authority is the person who first published a Latin
description of the species. A complete scientific name must consist of the Latin
binomial and the authority. Candy cassidony is a binomial while <em>Candy cassidony</em>
Ray is a scientific name. The word Candy is the generic name while the term
cassidony is the specific epithet. This naming practice cites the more general category first followed by the more specific. For instance my surname is Bellis and my given name is Vincent. I am Vincent of the Bellis clan. In botanical Latin my ‘binomial’ would be <em>Bellis vincent</em>. Botanists say the Bellis represents the generic, or genus, name. Vincent is the specific, or specific epithet.
</p>
<p>
Why the italics and why is the generic name capitalized while the specific epithet is in lower case? Remember that the scientific name is a condensed version of a longer Latin description. By convention we begin our sentences with a capital letter. Since the generic name was usually derived from the first word in the Latin description it was capitalized. The specific epithet was derived from the second word (in the descriptive sentence) and is not capitalized. Most learned works during this period were printed in an Italic font. The authority was printed in Roman font to more clearly distinguish it from the binomial. Remember that the ‘name at the end’ is a reference to an author or authority and is not part of the description of the plant.
</p>
<p>
This scientific and systematic approach to naming plants greatly helped reduce confusion. Botanists could now compare similar specimens and descriptions from various locations. Plants belonging to the same species but bearing different binomials could be combined under one name. Here is one example. The weed that we know as Dandelion was identified in several European languages by terms that described the individual tooth-like flowers (Leontodon = Lion’s tooth) of the plant. Linneaus gave this plant the binomial <em>Leontadon taraxacum</em>. Since Linnaeus was the authority the scientific name became <em>Leontadon taraxacum</em> L. The ‘L.’ refers to Linnaeus. A later specialist moved this plant to a new genus that he called Taraxacum. This authority was Frederich Heinrich Wiggers, 1746-1811. The accepted modern scientific name for the common dandelion is <em>Taraxicum officinale</em>. Wiggers. The specific name, <em>officinale</em>, refers to the wide use of this plant in herbal medicine. "Officinale" means "of the shops or herb markets."
</p>
<p>
When a major change occurs two authorities may appear in the scientific name. Botanists often examine old collections of dried plants, or herbaria, in order to gain a specialists understanding of a particular group. We see evidence of this on page 145-47 of the Lawson plants.<sup>4</sup> Information on this label shows that James L. Reveal of the University of Maryland examined this specimen sometime in 1989. He identified it as <em>Polypodium polypodioides</em> (L.) Watt. var <em>michauxianum</em> Weatherby. The information on this label indicates that this plant was first described by Linnaeus. He assigned it to a different genus. Later David Allen Poe Watt (1830-1917) transferred it to the genus Polypodium. Still later Charles Alfred Weatherby (1875-1949) recognized more than one variety of this species and designated this one as <em>michauxianum</em>. Andre Michaux was a French botanist who collected plants in eastern North America in the years just after the American Revolution. Michaux collected widely in western North Carolina. The French had helped push the English out of the United States and were interested in discovering new plants having potential commercial value.
</p>
<p>
The Linnaean System of Plant Classification was published in 1753 as <em>Species Plantarum</em>. This system remains the basis of modern plant classification and nomenclature. Over the years botanists have developed rules to govern the assignment of scientific names. One basic rule is that to be accepted as a valid name the description of a new species must be published in Latin in a recognized botanical publication. The rules governing plant names are published as The International Code of Botanical Nomenclature. Botanists periodically organize International Botanical Congresses in order to review the Code and to resolve disputes. The Royal Botanic Garden at Kew in London, England attempts to record all plant descriptions as they appear in the literature. The Index Kewensis lists all known specific epithets alphabetically under each genus.
</p>
<hr/>
<p>
<sup>1</sup> Note attached to a plant on page 242-129b in Lawson’s hand. "Feb. 18th, 1711. Evergreen. I formerly took this to be a sort of privett but am of the opinion it is ye gallberry, both bearing a black berry..." NOTE: This specimen has been identified as Large or Sweet Gallberry.
<br/>
<sup>2</sup> Note attached to a plant on page 171-65 in printed Latin. "12. <em>Candy Cassidony</em>. Ray 282.8 <em>Elychrysum Creticum</em> CB.264.6. <em>Chrysocome 5 qua Cretica Clus</em>. 327. <em>Chrysocome sive Staechas citrina Cretica Park</em>, 69.8. <em>Staechas citrina globoso &amp; amplo flore Cretica</em> Burrelier pl. 987.lc opt. 814. [This last author has given a very accurate Figure of this plant, which is so beautiful an ornament in our most curious Gardens.] NOTE: This plant has been identified as Rabbit Tobacco or Cudweed.
<br/>
<sup>3</sup> Ibid.
<br/>
<sup>4</sup> Note the annotation label at the bottom of p.145-47. This is an annotation label affixed to the herbarium specimen by an American fern specialist who was visiting the British Museum to examine the Lawson plants. NOTE: This plant has been identified as Resurrection Fern.
</p>

---
layout: ecu
title:  "Francis Latham Harriss, Biographical Sketch of John Lawson, Lawson's History of North Carolina, 1951"
customdate: "1951"
order: 18
---

{% include breadcrumbs.html %}

<h3>Francis Latham Harriss, Biographical Sketch of John Lawson, <em>Lawson's History of North Carolina</em>, 1951</h3>

<div class="well">
  <label>Text from Book</label><br/>

<p>
[Page xiii]
</p>
<p>
Biographical Sketch of John Lawson
</p>
<p>
THE proprietory period in North Carolina produced only one volume as its contribution to American colonial history, John Lawson's History of North Carolina, though, strictly speaking, it is not a history at all in the usually accepted sense. In offering a reprint of this book the editor believes that Americans are being given an opportunity to learn and understand the difficulties and dangers, the primitive luxuries and beauties of the wild which were endured and enjoyed by the men and women of high courage who met and overcame them, not to be found within the covers of any other one volume in such clear and complete detail. The narrative is enlivened constantly by a hearty and vivid humor and betrays in every line Lawson's youthful zest in the adventure, but this joyous current which carries the reader rapidly along from page to page does not conceal from his eyes the hardships and perils accompanying the daily life of the white man in the land of the Indian.
</p>
<p>
Of John Lawson himself little is known with certainty prior to his life in North Carolina. He appears to have flashed like a meteor across our ken, leaving behind him only this illuminating record of his presence and the tragic memory of his death. Where he was born, who were his parents, and where he was educated have not thus far been brought to light, though much scholarly effort has been expended upon the subject. Stephen B. Weeks Ph.D., of the United States Bureau of Education (1896) formed this definite conclusion from his extensive researches into the subject, that John Lawson came from the family of the Lawsons of Brough Hall, Yorkshire, England, and was probably "the son of that Lawson who was such a faithful adherent of the King in the civil war that he suffered the sequestration of his estates under the Commonwealth." In 1665, during the Restoration, his estates were returned to him, and he received Knighthood for his loyalty.
</p>
<p>
All we know of John Lawson indicates beyond doubt that he was
</p>
<p>
[Page xiv]
</p>
<p>
a gentleman by birth, that he was well educated, his tastes cultured, and that he possessed ample means to indulge them, and was free to choose the course of his life and steer it where he would. Being of a mind, as he tells us, to travel and see the world, he started to journey with the human mass crowding toward Rome to witness the pageant of the Pope's Jubilee, in the year 1700. But he "met a gentleman who had been abroad, and was very well acquainted with the ways of living in both Indies," and the course of his life was changed. The gentleman fired his imagination with the possibilities of adventure in the New World, so Lawson deserted the well known path across the old, familiar world, to pit his youthful strength and ardor against the mystery and unknown hazards of the new.
</p>
<p>
Five years after his arrival in the colony we find him one of the incorporators of Bath Town, the first town established in North Carolina. He remained in the colony for eight years before returning to England during which time he was engaged in surveying and encouraging colonization, as well as writing his History of North Carolina. In recognition of his valuable services he was appointed Surveyor-General of the Colony by the Lords Proprietors. His intimate associates at Bath were Christopher Gale, who became the first Chief Justice of North Carolina, and Captain Lionel Reading, who was apparently a cultured man as we find him appointed Trustee of Bath Library, a valuable collection of books provided by the celebrated Dr. Bray, and the first public library in North Carolina.
</p>
<p>
In 1709 Lawson was in London evidently attending to the publication of his book which made its first appearance in that year. With him on that visit were Christopher Gale and Lionel Reading. These two gentlemen are generally believed to have been endeavoring to straighten out with the home government the violent quarrel in progress at that time between Colonel Thomas Carey and William Glover, President of the Council. This quarrel was incited by the oath of allegiance to Queen Anne which the Quakers considered an unwarranted burden placed on their consciences which they refused
</p>
<p>
[Page xv]
</p>
<p>
to bear, and resulted in the uprising known as "Carey's Rebellion." While Lawson appears to have been tolerant and liberal in his attitude toward others, there is no evidence that he interested himself in the Carey troubles, or ever took part in political disputes. He was a very busy man, more interested in his orchards than in matters of colonial policy. His book is singularly free from allusion to political or personal affairs. It was while on this visit to England that Lawson was called upon by the Lords Proprietors to assist DeGraffenreid [von Graffenried], a Swiss adventurer, in the settlement of his colony of Palatines in North Carolina. That connection proved to be a most unfortunate one for John Lawson.
</p>
<p>
During January of the year 1710 the party of North Carolina gentlemen with seven hundred Palatines returned to America. DeGraffenreid [von Graffenried] remained in England. The misfortunes of the Palatines, with Lawson's struggles to assist them, their appeal to the Lords Proprietors against the injustices of DeGraffenreid [von Graffenried], and the latter's scurrilous accusations against Lawson after his death, form too long a story for this sketch.
</p>
<p>
It was in September, 1711, that Lawson started on his last trip of exploration. With him went DeGraffenreid [von Graffenried], two negroes, and two trusted Indians. Christopher had intended joining the expedition but, as he himself expressed it later, "the happy illness of my wife prevented me." The purpose of the trip was to discover how far up the Neuse River was navigable, and if it was possible to locate a better road in that direction to Virginia. At that time the Indian tribes had become encouraged to believe that the constant quarrels of the whites among themselves were an opportunity offered them to put up a fight to regain their lost lands and hunting grounds. Led by the savage Tuscaroras, they had been stealthily organizing their forces for some time. Lawson must have been aware to some extent of this condition of affairs, but the strength of the movement was a monstrous surprise to everyone. Conscious of his kindness and fair dealing with the Indians, and being an absolutely fearless man, he evidently saw no danger to himself in moving freely
</p>
<p>
[Page xvi]
</p>
<p>
about the Indian country attending to his own affairs. He had, however, been appointed an associate of Edward Mosely in surveying the disputed boundary line between North Carolina and Virginia. His work as surveyor brought him into constant contact with the Indians. Stirred as they were by bitter thoughts of their losses and watching him at work, they mistook him for the cause of their despoiling, ignorant of the truth that he was only an agent. Apparently the possibility of such a feeling against him had not occurred to his mind.
</p>
<p>
The only account we have of this fatal trip up the Neuse River is from the pen of DeGraffenreid [von Graffenried], when he was trying to explain his conduct of affairs to the Lords Proprietors. He does not fail to use that pen entirely to his own advantage and in cold disparagement of a man forever silenced by a cruel death. DeGraffenreid recounts at length how the little party slowly made their way up the river. Suddenly they were surrounded and seized by a number of Indians who led them away to King Hencock's [Hancock's] town of Catechna, where they were tried, acquitted, and ordered set at liberty next day. Meantime other prominent Indians arrived who wanted to know why these white men were being set at liberty. DeGraffenreid asserts that at this critical time Lawson ruined their prospects by quarreling with the King of the Corees. This is difficult to understand, because it is so entirely out of keeping with what we know of Lawson's character. However, they were tried again and sentenced to death. Many indignities were heaped upon them, their periwigs torn from their heads and thrown in the fire. Finally they were led away under heavy guard. Many pages are filled by DeGraffenreid [von Graffenried] in explaining how he escaped the result of this death sentence, and shifting all the blame and responsibility for Lawson's death upon the shoulders of Lawson himself. Presumably the negro slaves suffered death with Lawson, though DeGraffenreid [von Graffenried] does not mention them at all. He informs us that he declared himself to the Indians as being under the particular care and protection of the Great White Queen, who would terribly avenge him if any harm came to him. Why no
</p>
<p>
[Page xvii]
</p>
<p>
such consideration prevented the awful slaughter of her subjects visited upon the white settlements by these Indians a few days later the Baron does not attempt to explain.
</p>
<p>
John Lawson was left alone to face torture and death. In what manner, no one knows. Christopher Gale believed that he died after the manner described in his History of North Carolina, with the pitch pine splinters stuck all over his body and lighted as torches, a living column of fire until released by the mercy of death. The offices of trust which he held, the character of the men who were his friends, the records of his dealings given in the Colonial Records of North Carolina, and the Minutes of the Lords Proprietors, the kindly tolerance for the savages as expressed in his book, his sympathy for their ignorance and desire for fair dealing towards them on the part of the "Christians" are clear evidece of the manner of man he was. We may be sure that he died as he had lived, a gallant gentleman.
</p>
<p>
FRANCES LATHAM HARRISS.
</p>
<p>
Wilmington, North Carolina
</p>
<p>
November, 1937.
</p>
</div>



<table class="table">
  <tr>
    <td>Citation:</td>
    <td>Harriss, Francis Latham. "Biographical Sketch of John Lawson." <em>Lawson's History of North Carolina</em>. By John Lawson. 1709. Ed. Francis Latham Harriss. Richmond, VA: Garrett and Massie, 1937. xiii-xvii.</td>
  </tr>
  <tr>
    <td>Location:</td>
    <td>North Carolina Collection, Joyner Library, East Carolina University, Greenville, NC 27858 USA</td>
  </tr>
  <tr>
    <td>Call Number:</td>
    <td>NoCar F 257 L447</td>
  </tr>
</table>
